const express = require('express')
const Commande = require('../model/commande')

var router = express.Router();

router.post('/ajouter', function name(req, res) {
    const {produits, email_utilisateur, prix_total} = req.body;
    const commande = new Commande({produits, email_utilisateur, prix_total});

    commande.save()
    .then(data =>{
        res.send(data);
    })
    .catch(err => {
        res.status('500').send({'message':`error creating order ${err.message}`});
    });

});
router.get('/acheter', function name(req,res) {
    Commande.find()
    .then(data =>{
        res.send(data);
    })
    .catch(err => {
        res.status('500').send({'message':`cannot find orders ${err.message}`});
    });

});


module.exports = router